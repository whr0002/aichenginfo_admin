﻿var app = angular.module('AdminApp', []);

app.service('businessService', function ($http) {
    this.getUnverifiedBusinesses = function (successCallback) {
        $http.get('http://yezi-api.azurewebsites.net/yezi/api/rest/v1/business-users/FOOD').success(function (data) {
            successCallback(data);
        });
    };

    this.verify = function (id) {
        alert("id: " + id);
    };

});

app.controller('businessController', ['$scope', 'businessService', function ($scope, businessService) {
    $scope.verify = businessService.verify;
    $scope.businessList = [];
    $scope.isInEditMode = false;

    businessService.getUnverifiedBusinesses(function (data) {
        $scope.businessList = data;
    });
}]);

